//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by MacStudent on 2019-09-30.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    var pikachu : SKSpriteNode!
    var square : SKSpriteNode!
    var highScoreLabel : SKLabelNode!

    // CONSTRUCTOR - Setup your scene + sprites
    // --------------------------------------------
    override func didMove(to view: SKView) {
        print("HELLO WORLD!")
        // size = a global variable that gives you the size of screen
        // size.width = this.screenwidth in Andrdoid
        // size.height = this.screenHeight in Android
        print("Screen size (w x h): \(size.width), \(size.height)")
        
        
        // ADD SOME SPRITES
        // ---------------
        
        // Make some text
        // ------------------
        
        // 1. Make a label node
        highScoreLabel = SKLabelNode(text: "Score: 25")
        
        // 2. Configure the node
        // (Setting the font size, color, position, etc)
        highScoreLabel.position = CGPoint(x: 100, y: 100)
        highScoreLabel.fontSize = 45
        highScoreLabel.fontColor = UIColor.yellow
        highScoreLabel.fontName = "Avenir"
        
        // 3. Show the node on the screen
        addChild(highScoreLabel)
        
        
        // Draw a square
        // ------------------
        
        // 1. Create a node for the square
        // When you make a squre, use the SKSpriteNode object
        // Note: CGSize objects = Rect object in java
        self.square = SKSpriteNode(color: UIColor.yellow,
                                  size: CGSize(width: 100, height: 100))
        
        // 2. Configure the square
        self.square.position = CGPoint(x: 200, y: 500)
        
        // 3. Add the square to the screen
        addChild(square)
        
        
        
        // Draw a picture
        // ------------------
        
        // 1. Make an image node
        self.pikachu = SKSpriteNode(imageNamed: "pikachu64")
        
        // 2. configure the image node
        self.pikachu.position = CGPoint(x:200, y: 323)
        
        // 3. Add the image node to the screen
        addChild(pikachu)
    }
    
    // UPDATEPOSITIONS/REDRAW SPRITES FUNCTION
    // --------------------------------------------
    // Built in function that runs once per frame
    // Similar to updatePositions() in Android
    
    var pikachuDirection = "right"
    override func update(_ currentTime: TimeInterval) {
        // print("\(currentTime): NONSENSE")
        
        if(self.pikachuDirection == "right")
        {
            self.pikachu.position.x = self.pikachu.position.x + 1
            if(self.pikachu.position.x >= size.width){
                self.pikachuDirection = "left"
            }
        }
        if(self.pikachuDirection == "left")
        {
            self.pikachu.position.x = self.pikachu.position.x - 1
        }
        if(self.pikachu.position.x <= 0){
            self.pikachuDirection = "right"
        }
        let upMoveAction = SKAction.moveBy(x: 0, y: 1, duration: 2)
        let leftMoveAction = SKAction.moveBy(x: -1, y: 0, duration: 2)
        self.square.run(upMoveAction)
        self.highScoreLabel.run(leftMoveAction)

        
    }
    
    
    // DETECT USER INPUT
    // --------------------------------------------
    
    
    // touchesBEGAN = Event.ACTION_DOWN
    // Use this if you want to detect when finger TOUCHES screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    // touchesENDED = Event.ACTION_UP
    // Use this if you want to detect when finger LIFTS from screen
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("PERSON TOUCHED THE SCREEN")
        
        // Get the (x,y) position of the mouse
        // SpriteKit will return the position as a UITouch object
        let locationTouched = touches.first
        
        if (locationTouched == nil) {
            // some kind of error occured when detecting the touch location
            //@antoniopolice, yes i probably should use some kind of guard-let here
            return
        }
        
        // Inside the UITouch object, look at the location property
        // The location property specifies WHAT thing you are touching
        let mousePosition = locationTouched!.location(in: self)
       
        // Then, get the (x,y) coordinates
        print("x = \(mousePosition.x)")
        print("y = \(mousePosition.y)")
        print("-----------")
    }
    
    
}
